
var User = require('../models/users.js');
// var Loker = require('../models/ipfs.js')
var Mail = require('../models/SendMail.js');
var SMS = require('../models/SendSms.js');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var config = require('../../config/passport_config.js');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var formidable = require("formidable");
var crypto = require('crypto');
var HttpStatus = require('http-status-codes');
var GlobalMessages = require('../../config/constantMessages');
var messageHandler = require('../../config/messageHandler');
var multer  =   require('multer');
const fileUpload = require('express-fileupload');

var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var async = require('async');
/*________________________________________________________________________
 * @Date:      	10 Nov,2017
 * @Method :   	Register
 * Modified On:	-
 * @Purpose:   	This function is used for sign up user.
 _________________________________________________________________________
 */

var createFile = function(req, res){

    console.log('totalComponents:::',req.body.totalComponent);
    var totalComponents = req.body.totalComponent;
    var componentName = req.body.componentName;
    var basePath = '/home/kunvar/';


    var filePath = basePath+componentName+"/"+componentName+'.component.ts';
    var htmlfilePath = basePath+componentName+"/"+componentName+'.component.html';
    var cssfilePath = basePath+componentName+"/"+componentName+'.component.css';

    var tsContent = ``;
    var htmlContent =``;
    var obj = {html : '', ts: ''};

    if (!fs.existsSync(basePath+componentName)){
        fs.mkdirSync(basePath+componentName);
    }else{

    }

    async.waterfall([
        function(callback) {
            if(totalComponents){
                if(totalComponents.textbox){
                    console.log("textbox::",totalComponents.textbox.length);
                    for (var i = 0; i< totalComponents.textbox.length; i++) {
                        tsContent += totalComponents.textbox[i].modelName+" : any;"+"\n";
                        htmlContent += '<input type="text" name="'+totalComponents.textbox[i].modelName+'" placeholder="'+totalComponents.textbox[i].modelName+'" [(ngModel)]="'+totalComponents.textbox[i].modelName+'"/>';
                    }
                }

            }
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(contentTs,contentHtml, callback){
            tsContent = contentTs;
            htmlContent = contentHtml;
            console.log("textarea",obj.html);
            if(totalComponents.textArea){
                    console.log("textarea::",totalComponents.textArea.length);
                    for (var i = 0; i< totalComponents.textArea.length; i++) {
                      tsContent += totalComponents.textArea[i].modelName+" : any;"+"\n";
                      htmlContent += '<input type="text"  name="'+totalComponents.textArea[i].modelName+'" placeholder="'+totalComponents.textArea[i].modelName+'" [(ngModel)]="'+totalComponents.textArea[i].modelName+'"/>';
                     }
            }
            obj.html = htmlContent;
            callback(null, tsContent);
        },
        function(content, callback){
            tsContent = content;
            if(totalComponents.button){
                    console.log("button::",totalComponents.button.length);
                    for (var i = 0; i< totalComponents.button.length; i++) {
                       tsContent += totalComponents.button[i].defination+"\n"
                       htmlContent += '<input type="button" (click)="'+totalComponents.button[i].buttonName+'()" name="'+totalComponents.button[i].buttonName+'" value="'+totalComponents.button[i].buttonName+'"/>';
                    }
            }
            console.log("button",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.image){
                    console.log("image::",totalComponents.image.length);
                    for (var i = 0; i< totalComponents.image.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<img src="'+totalComponents.image[i].src+'" alt="No image avaiable"/>';
                    }
            }
            console.log("image",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.column1){
                    console.log("column1::",totalComponents.column1.length);
                    for (var i = 0; i< totalComponents.column1.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<div src="'+totalComponents.column1[i].column1+'" class="cloumn1"><div/>';
                    }
            }
            console.log("column1",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.column2){
                    console.log("column2::",totalComponents.column2.length);
                    for (var i = 0; i< totalComponents.column2.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<div src="'+totalComponents.column2[i].column2+'" class="cloumn2"><div/>';
                    }
            }
            console.log("column2",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.column3){
                    console.log("column3::",totalComponents.column3.length);
                    for (var i = 0; i< totalComponents.column3.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<div src="'+totalComponents.column3[i].column3+'" class="cloumn3"><div/>';
                    }
            }
            console.log("column3",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.column4){
                    console.log("column4::",totalComponents.column4.length);
                    for (var i = 0; i< totalComponents.column4.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<div src="'+totalComponents.column4[i].column4+'" class="cloumn4"><div/>';
                    }
            }
            console.log("column4",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        }
    ], function (err, result, result1) {
    // console.log("result result1", result1);
    // Make HTML component---------------------------------
    if (fs.existsSync(htmlfilePath)) {
        fs.unlink(htmlfilePath,function(err){
                    if(err) return console.log(err);
                    createHtmlFile(htmlfilePath,result1,componentName, req, res);
            });
    }
    else{
        createHtmlFile(htmlfilePath,result1,componentName, req, res);
    }

    // Make TS component -----------------------------
    if (fs.existsSync(filePath)) {
            // if file exists then delete it and make new one
            fs.unlink(filePath,function(err){
                    if(err) return console.log(err);
                     createTsFile(filePath,result,componentName, req, res);
            });
    }else{
        createTsFile(filePath,result,componentName, req, res);
    }

    // Make CSS component -----------------------------
    if (fs.existsSync(cssfilePath)) {
            fs.unlink(cssfilePath,function(err){
                    if(err) return console.log(err);
                     createCSSFile(cssfilePath,'', req, res);
            });
    }else{
        createCSSFile(cssfilePath,'', req, res);
    }

    });
}

function createTsFile(filePath,fileContent,componentName, req, res){
    var startTs = `import { Component,OnInit } from '@angular/core';
                    import { Http } from '@angular/http';
                      @Component({
                        selector: '`+componentName+`-root',
                        templateUrl: './`+componentName+`.component.html',
                        styleUrls: ['./`+componentName+`.component.css']
                      })
                      export class `+componentName[0].toUpperCase() + componentName.substring(1)+`Component implements OnInit {
                        constructor(private http : Http){};
                        ngOnInit(){}
                        title = '`+componentName+`';
                        `+fileContent+`
                    }`;
                    fs.appendFile(filePath,startTs,function(data){
                        console.log("TS file createed successfully!.");
                    });
}

function createHtmlFile(htmlfilePath,fileContent,componentName, req, res){
    var startHtml = `<!DOCTYPE html>
                 <html>
                 <title>`+componentName+` Page</title>
                 <style>.wrapper{ width: 600px; margin:0px auto; padding:40px; box-shadow: 0px 0px 7px #ededed;} </style>
                 <body class="wrapper">
                `+fileContent+`
                 </body>
                </html>`;
            fs.appendFile(htmlfilePath, startHtml ,function(data){
                console.log("HTML file createed successfully!.");
                res.send({status: 200, message:"Component downloaded successfully!."});
            });
}

function createCSSFile(cssfilePath,fileContent, req, res){
    var startCss = `.body {

                     }
                    `;
            fs.appendFile(cssfilePath, startCss ,function(data){
                console.log("CSS file createed successfully!.");
            });
}

// Api for create project
var createProject = function(req, res){
    var projectName = req.body.projectName;
    var projectFolder = '/home/kunvar/'+projectName;

    if (!fs.existsSync(projectFolder)){
        fs.mkdirSync(projectFolder);
    }else{

    }

    var packageJSON = `{
  "name": "`+projectName+`",
  "version": "0.0.0",
  "scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e"
  },
  "private": true,
  "dependencies": {
    "@angular/animations": "^6.0.0",
    "@angular/common": "^6.0.0",
    "@angular/compiler": "^6.0.0",
    "@angular/core": "^6.0.0",
    "@angular/forms": "^6.0.0",
    "@angular/http": "^6.0.0",
    "@angular/platform-browser": "^6.0.0",
    "@angular/platform-browser-dynamic": "^6.0.0",
    "@angular/router": "^6.0.0",
    "@ng-bootstrap/ng-bootstrap": "^2.2.0",
    "@tinymce/tinymce-angular": "^2.0.0",
    "@types/file-saver": "^1.3.0",
    "angular2-color-picker": "^1.3.1",
    "core-js": "^2.5.4",
    "file-saver": "^1.3.8",
    "jquery": "^3.3.1",
    "ng2-drag-drop": "^3.0.2",
    "rxjs": "^6.0.0",
    "rxjs-compat": "^6.2.2",
    "zone.js": "^0.8.26"
  },
  "devDependencies": {
    "@angular/compiler-cli": "^6.0.0",
    "@angular-devkit/build-angular": "~0.6.0",
    "typescript": "~2.7.2",
    "@angular/cli": "~6.0.0",
    "@angular/language-service": "^6.0.0",
    "@types/jasmine": "~2.8.6",
    "@types/jasminewd2": "~2.0.3",
    "@types/node": "~8.9.4",
    "codelyzer": "~4.2.1",
    "jasmine-core": "~2.99.1",
    "jasmine-spec-reporter": "~4.2.1",
    "karma": "~1.7.1",
    "karma-chrome-launcher": "~2.2.0",
    "karma-coverage-istanbul-reporter": "~1.4.2",
    "karma-jasmine": "~1.1.1",
    "karma-jasmine-html-reporter": "^0.2.2",
    "protractor": "~5.3.0",
    "ts-node": "~5.0.1",
    "tslint": "~5.9.1"
  }
}`;
    fs.appendFile(projectFolder+'/package.json', packageJSON,function(data){
        res.send({status: 200, message:"Project created successfully!."});
    });
}

var createElement = function(req, res){
    var elementType = req.body.type;
    var count = req.body.count;
    var tsContent = ``;
    switch(elementType){
        case 'input' :
            var content = '<input type="text" name="textbox" id="textbox">';
            tsContent = 'dynamic_text_'+count+" : any;";
            break;

        case 'button' :
            var content = '<input type="button" name="textbox" id="textbox">';
            tsContent = `dynamic_button_`+count+`(){

            };`;
            break;

        case 'textArea' :
             var content = '<input type="text" name="textbox" id="textbox">';
             tsContent = 'dynamic_text_'+count+" : any;";
            break;
    }

    var filePath = '/home/kunvar/login.component.ts';

            if (fs.existsSync(filePath)) {

            fs.readFile(filePath, function(err, data) {
                if(err) throw err;
                    theFile = data.toString().split("\n");
                    theFile.splice(-1,1);

                    theFile[theFile.length] = tsContent+"\n }";

                    fs.writeFile(filePath, theFile.join("\n"), function(err) {
                    if(err) {
                        return console.log(err);
                    }
                    console.log("Removed last 1 lines");
                    console.log(theFile.length);
                    res.send({status: 200, message:"Element createed successfully!."});
               });
            });

        // fs.appendFile('/home/kunvar/login.component.ts',tsContent,function(data){
            // res.send({status: 200, message:"Element createed successfully!."});
         // });

    }else{
        var start = `import { Component } from '@angular/core';
          @Component({
            selector: 'login-root',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
          })
          export class LoginComponent {
            constructor(){};
            title = 'login';
            `+tsContent+`
        }`;
        fs.appendFile('/home/kunvar/login.component.ts',start,function(data){
            res.send({status: 200, message:"Element createed successfully!."});
        });
    }


}

var check = function (req,res){
    res.send({name:req.body.name});
}

var updateMany = function (req,res){

    User.updateMany(
        {key: req.body.key},
        {$set: { "gender": req.body.gender}},
        function (err,data) {
            if (err) {
                res.status(HttpStatus.NOT_FOUND).send({msg:err,status:HttpStatus.NOT_FOUND});
            }
            else if(data){
                res.send({msg:'Updated',status:400,data : data});
            }
        });
}

var getUpdatedValues = function (req,res){

    User.findOneAndUpdate(
        { email: req.body.email },
        { $set: {"key": req.body.key} },
        { new: true },
        function (err,data) {
            if (err) {
                res.send({msg: err, status: 400 });
            }
            else if(data){
                res.send({msg:'Updated',status:400, data : data});
            }
        });
}

var register = function (req, res) {
    var user = {};
    user = req.body;
    console.log(req.body, 'user');
    var token;
    // var condition = { $and: [ { email: { $ne: 1.99 } }, { price: { $exists: true } } ] };
console.log("data");
    if(!user || !user.email || !user.password ) {
        res.send({msg:"Please provide all the details",status:400})
    } else {
        User.findOne({email: user.email,key : user.key},{}, function (err,data) {
            if (err) {
                res.send({msg:err,status:400});
            }
            else if(data){
                res.send({msg:"Someone is already using email: "+user.email+" and key: "+user.key,status:400});
            } else {
              if(req.body.password != req.body.confirmpassword )
                {
                return  res.json({status:400,responseMessage:"password and confirmpassword are not match"});
              }else{
                    crypto.randomBytes(10, function (err, buf) {
                    token = buf.toString('hex');
                    user.verificationToken = token;
                    user.verifyEmail = {
                        email: req.body.email.toLowerCase(),
                        verificationStatus: false
                    };
                    var errorMessage = "";
                    User(user).save(function (err, data) {
                        if (err) {
                             res.send(messageHandler.errMessage(err));
                        } else {
                            var verifyurl = 'api/verifyemail/' + user.verificationToken;
                            Mail.registerMail(user,verifyurl, function(msg) {
                                console.log('Mail sent successfully.')
                            });
                            res.send({msg: "you have registered successfully",status:200});
                        }
                    });
                });
              }
            }
        });
    }
}

/*________________________________________________________________________
 * @Date:       10 Nov,2017
 * @Method :    verifyEmail
 * Modified On: -
 * @Purpose:    This function is used to verify user.
 _________________________________________________________________________
 */

var verifyEmail = function (req, res) {
    User.findOne({verificationToken: req.params.token}, function (err, data) {
        if (err) {
            res.status(203).send({msg: "Something went wrong."});
        } else {
            if (!data) {
                res.status(203).send({msg: "Token is expired."});
            } else {
                var verificationStatus = data.verifyEmail.verificationStatus;
                var user_id = data._id;
                if (verificationStatus === true) { // already verified
                    console.log("account verified");
                    res.status(200).send({msg: "Account Already verified."});
                } else { // to be verified
                    data.email = data.verifyEmail.email;
                    data.verifyEmail = {
                        email: data.verifyEmail.email,
                        verificationStatus: true
                    };
                    data.save(function (err, data) {
                        if (err) {
                            res.status(203).send({msg: "Something went wrong."});
                        } else {
                            Mail.verifyAccountMail(data.email, function (msg) {
                                console.log('Mail sent successfully.')
                            });
                            res.status(200).send({msg: "you have verified your account successfully"});
                        }
                    });
                }
            }
        }
    });
};
/*________________________________________________________________________
 * @Date:       10 Nov,2017
 * @Method :    login
 * Modified On: -
 * @Purpose:    This function is used to authenticate user.
 _________________________________________________________________________
 */

var login = function (req, res) {
  console.log("snsjsjsjsjsjsjsj");
    var user = req.body;
    if (!user || !user.email) {
        res.send({msg: "Please provide valid email and password",status:HttpStatus.NOT_FOUND});
    } else {
        User.findOne({email: user.email},
            {}, function (err, data) {
                if (err) {
                    res.send({msg: err,status:HttpStatus.NOT_FOUND});
                } else {
                    if(data){
                        if(data.verifyEmail.verificationStatus == false) {
                            res.send({msg: "Your email is not verified.",status:HttpStatus.NOT_FOUND});
                        }else {
                            if (data) {
                                bcrypt.compare(user.password, data.password, function (err, result) {
                                    if (err) {
                                        res.send({msg: err,status:HttpStatus.NON_AUTHORITATIVE_INFORMATION});
                                    } else {
                                        if (result === true) {
                                            data.active = true;
                                            data.lastSeen = new Date().getTime();
                                            data.save(function (err, success) {
                                                if (!err) {
                                                    var token = jwt.sign({_id: data._id}, config.secret);
                                                    // to remove password from response.
                                                    data = data.toObject();
                                                    delete data.password;
                                                    res.json({msg:"user login successfully",status:HttpStatus.OK,data : data});
                                                }
                                            });
                                        } else {
                                            res.send({msg: 'Authentication failed due to wrong details.',status:HttpStatus.NOT_FOUND});
                                        }
                                    }
                                });
                            } else {
                                res.send({msg: 'No account found with given email.',status:HttpStatus.NOT_FOUND});
                            }
                    }
                }
                else{
                   res.send({msg: "Your email is not register with us. Please signup first",status:HttpStatus.NOT_FOUND});
                }
            }
        });
    }
};


/*________________________________________________________________________
 * @Date:       10 Nov,2017
 * @Method :    forgot_password
 * Modified On: -
 * @Purpose:    This function is used when user forgots password.
 _________________________________________________________________________
 */
var forgotPassword = function (req, res) {
    crypto.randomBytes(10, function (err, buf) {
        var token = buf.toString('hex');
        User.findOne({email: req.body.email}, function (err, data) {
            if (err) {
                res.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).send({msg: 'Please enter a valid email.',status:HttpStatus.NON_AUTHORITATIVE_INFORMATION});
            } else if (!data) {
                res.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).send({msg: 'Email does not exist.',status:HttpStatus.NON_AUTHORITATIVE_INFORMATION});
            } else {
                if (data) {
                    data.resetPasswordToken = token,
                    data.resetPasswordExpires = Date.now() + 3600000;

                    data.save(function (err, data) {
                        if (err) {
                            res.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).send({msg: 'Something went wrong.',status:HttpStatus.NON_AUTHORITATIVE_INFORMATION});
                        } else {
                            Mail.resetPwdMail(req.body, token, function (msg) {
                                console.log('Reset password mail sent successfully.')
                            });
                        }
                        res.status(HttpStatus.OK).send({msg: 'Email sent successfully.',status:HttpStatus.OK});
                    });
                }
            }
        });

    });
};


/*________________________________________________________________________
 * @Date:       10 Nov,2017
 * @Method :    resetPassword
 * Modified On: -
 * @Purpose:    This function is used when user reset password.
 _________________________________________________________________________
 */


var resetPassword = function (req, res) {
    if (req.body.newPassword && req.body.token) {
        User.findOne({resetPasswordToken: req.body.token}, function (err, data) {
            if (err) {
                res.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).send({msg: 'No record found.',status:HttpStatus.NON_AUTHORITATIVE_INFORMATION});
            } else {
                if (!data) {
                    res.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).send({msg: 'Reset Password token has been expired.',status:HttpStatus.NON_AUTHORITATIVE_INFORMATION});
                } else {

                    data.password = req.body.newPassword;
                    data.resetPasswordToken = undefined;
                    data.resetPasswordExpires = undefined;

                    data.save(function (err, data) {
                        if (err) {
                            res.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).send({msg: 'No record found.',status:NON_AUTHORITATIVE_INFORMATION});
                        } else {
                            Mail.resetConfirmMail(data, function (msg) {
                                console.log('Reset Confirmation mail sent successfully.')
                            });
                            res.status(HttpStatus.OK).send({msg: 'Password has been successfully updated.',status:HttpStatus.OK});
                        }
                    });
                }
            }
        });
    }
    else{
        res.status(HttpStatus.BAD_REQUEST).send({msg: GlobalMessages.CONST_MSG.fillAllFields,status:HttpStatus.BAD_REQUEST});
    }
};

/*________________________________________________________________________
 * @Date:       16 Nov,2017
 * @Method :    imageUpload
 * Modified On: -
 * @Purpose:    This function is used when user reset password.
 _________________________________________________________________________
 */
var storage =   multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, './uploads');
      },
      filename: function (req, file, callback) {
        callback(null, file.fieldname + '-' + Date.now());
      }
    });
    var upload = multer({ storage : storage},{limits: {
          fieldNameSize: 100,
          files: 2,
          fields: 5
    }}).single('userPhoto');

 var imageUpload = function (req, res) {
     upload(req,res,function(err) {
        if(err) {
            return res.send({msg:GlobalMessages.CONST_MSG.fileUploadError,err:err.message});
        }
        res.send({msg:GlobalMessages.CONST_MSG.fileUploadSuccess, status:HttpStatus.OK});
    });

 }


var createFile1 = function(req, res){

    console.log('totalComponents:::',req.body.totalComponent);
    var totalComponents = req.body.totalComponent;
    var componentName = req.body.componentName;
    var basePath = '/home/kunvar/';


    var filePath = basePath+componentName+"/"+componentName+'.component.ts';
    var htmlfilePath = basePath+componentName+"/"+componentName+'.component.html';
    var cssfilePath = basePath+componentName+"/"+componentName+'.component.css';

    var tsContent = ``;
    var htmlContent =``;
    var obj = {html : '', ts: ''};

    if (!fs.existsSync(basePath+componentName)){
        fs.mkdirSync(basePath+componentName);
    }else{

    }

    async.waterfall([
        function(callback) {
            if(totalComponents){
                if(totalComponents.textbox){
                    console.log("textbox::",totalComponents.textbox.length);
                    for (var i = 0; i< totalComponents.textbox.length; i++) {
                        tsContent += totalComponents.textbox[i].modelName+" : any;"+"\n";
                        htmlContent += '<input type="text" formControlName="'+totalComponents.textbox[i].modelName+'" name="'+totalComponents.textbox[i].modelName+'" placeholder="'+totalComponents.textbox[i].modelName+'" [(ngModel)]="'+totalComponents.textbox[i].modelName+'"/>';
                    }
                }

            }
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(contentTs,contentHtml, callback){
            tsContent = contentTs;
            htmlContent = contentHtml;
            console.log("textarea",obj.html);
            if(totalComponents.textArea){
                    console.log("textarea::",totalComponents.textArea.length);
                    for (var i = 0; i< totalComponents.textArea.length; i++) {
                      tsContent += totalComponents.textArea[i].modelName+" : any;"+"\n";
                      htmlContent += '<input type="text" formControlName="'+totalComponents.textArea[i].modelName+'" name="'+totalComponents.textArea[i].modelName+'" placeholder="'+totalComponents.textArea[i].modelName+'" [(ngModel)]="'+totalComponents.textArea[i].modelName+'"/>';
                     }
            }
            obj.html = htmlContent;
            callback(null, tsContent);
        },
        function(content, callback){
            tsContent = content;
            if(totalComponents.button){
                    console.log("button::",totalComponents.button.length);
                    for (var i = 0; i< totalComponents.button.length; i++) {
                       tsContent += totalComponents.button[i].defination+"\n"
                       htmlContent += '<input type="button" (click)="'+totalComponents.button[i].buttonName+'()" name="'+totalComponents.button[i].buttonName+'" value="'+totalComponents.button[i].buttonName+'"/>';
                    }
            }
            console.log("button",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.image){
                    console.log("image::",totalComponents.image.length);
                    for (var i = 0; i< totalComponents.image.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<img src="'+totalComponents.image[i].src+'" alt="No image avaiable"/>';
                    }
            }
            console.log("image",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.column1){
                    console.log("column1::",totalComponents.column1.length);
                    for (var i = 0; i< totalComponents.column1.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<div src="'+totalComponents.column1[i].column1+'" class="cloumn1"><div/>';
                    }
            }
            console.log("column1",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.column2){
                    console.log("column2::",totalComponents.column2.length);
                    for (var i = 0; i< totalComponents.column2.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<div src="'+totalComponents.column2[i].column2+'" class="cloumn2"><div/>';
                    }
            }
            console.log("column2",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.column3){
                    console.log("column3::",totalComponents.column3.length);
                    for (var i = 0; i< totalComponents.column3.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<div src="'+totalComponents.column3[i].column3+'" class="cloumn3"><div/>';
                    }
            }
            console.log("column3",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        },
        function(content, contentHtml, callback){
          tsContent = content;
          htmlContent = contentHtml;
            if(totalComponents.column4){
                    console.log("column4::",totalComponents.column4.length);
                    for (var i = 0; i< totalComponents.column4.length; i++) {
                       // tsContent += totalComponents.image[i].defination+"\n"
                       htmlContent += '<div src="'+totalComponents.column4[i].column4+'" class="cloumn4"><div/>';
                    }
            }
            console.log("column4",htmlContent);
            obj.html = htmlContent;
            callback(null, tsContent, htmlContent);
        }
    ], function (err, result, result1) {
    // console.log("result result1", result1);
    // Make HTML component---------------------------------
    if (fs.existsSync(htmlfilePath)) {
        fs.unlink(htmlfilePath,function(err){
                    if(err) return console.log(err);
                    createHtmlFile(htmlfilePath,result1,componentName, req, res);
            });
    }
    else{
        createHtmlFile(htmlfilePath,result1,componentName, req, res);
    }

    // Make TS component -----------------------------
    if (fs.existsSync(filePath)) {
            // if file exists then delete it and make new one
            fs.unlink(filePath,function(err){
                    if(err) return console.log(err);
                     createTsFile(filePath,result,componentName, req, res);
            });
    }else{
        createTsFile(filePath,result,componentName, req, res);
    }

    // Make CSS component -----------------------------
    if (fs.existsSync(cssfilePath)) {
            fs.unlink(cssfilePath,function(err){
                    if(err) return console.log(err);
                     createCSSFile(cssfilePath,'', req, res);
            });
    }else{
        createCSSFile(cssfilePath,'', req, res);
    }

    });
}

function createTsFile1(filePath,fileContent,componentName, req, res){
    var startTs = `import { Component,OnInit } from '@angular/core';
                    import { Http } from '@angular/http';
                    import { FormsModule, FormControl, FormBuilder, Validators, FormGroup, ReactiveFormsModule} from '@angular/forms';
                      @Component({
                        selector: '`+componentName+`-root',
                        templateUrl: './`+componentName+`.component.html',
                        styleUrls: ['./`+componentName+`.component.css']
                      })
                      export class `+componentName[0].toUpperCase() + componentName.substring(1)+`Component implements OnInit {
                        constructor(private http : Http,private fb : FormBuilder){};
                        title = '`+componentName+`';
                          ngOnInit() {
                            this.`+componentName+` = this.fb.group({
                              username : new FormControl('',Validators.required),
                              password :''
                            })
                          }
                        `+fileContent+`
                    }`;
                    fs.appendFile(filePath,startTs,function(data){
                        console.log("TS file createed successfully!.");
                    });
}

function createHtmlFile1(htmlfilePath,fileContent,componentName, req, res){
    var startHtml = `<!DOCTYPE html>
                 <html>
                 <title>`+componentName+` Page</title>
                 <style>.wrapper{ width: 600px; margin:0px auto; padding:40px; box-shadow: 0px 0px 7px #ededed;} </style>
                 <body class="wrapper">
                 <div [formGroup]="`+componentName+`">
                `+fileContent+`
                 </div>
                 </body>
                </html>`;
            fs.appendFile(htmlfilePath, startHtml ,function(data){
                console.log("HTML file createed successfully!.");
                res.send({status: 200, message:"Component downloaded successfully!."});
            });
}
var getProfile =  function(req,res) {
    console.log(req.user);
    var uid = req.body.userid;
    console.log("uid"+uid);
    if(!uid){
        res.send({msg:"OOPS! Something went wrong. Please try again"});
} else {
        User.findOne({_id:uid},{email :1,mobileNumber :1,gender: 1,name :1},function (err,data) {
            if(err) {
                res.status(400).send({msg:err})
            }else {
                if(data){
                    data.active = true;
                    data.lastSeen = new Date().getTime();
                    data.save(function (err,success) {
                        if(err){
                            console.log(err,"saveing error");
                        }else {
                            res.status(200).json(data);
                        }
                    });
                }else{
                    res.status(200).json({msg:"No user found"})
                }

            }
        })
    }
};
var userupdate = function(req,res){
  console.log("update||||||||||");
var updatevalue = req.body.updatename;
var uid = req.body.userid;
var no = req.body.updatemobile;
if(!uid && !updatevalue){
    res.send({msg:"OOPS! Something went wrong. Please try again"});
} else{
User.updateOne({_id:uid},
   {$set: {name:updatevalue,mobileNumber:no}}).then((success)=>
   res.json({
        "msg": "Profile has been update successfully!.",
        status:200
      }))
      .catch((error)=>res.json({
        "msg":"something went wrong",
        status:400
        })
      )
    }
}
var logout = function (req, res) {
    var uid = req.body.id;
    User.findOne({_id:uid},{lastSeen:1,active:1},function (err,data) {
        if(err) {
            res.status(400).send({msg:err})
        }else {
            if(data){
                data.active = false;
                data.lastSeen = new Date().getTime();
                data.save(function (err,success) {
                    if(err){
                        console.log(err,"saveing error");
                    }else {
                      res.json({
                           "message": "logout successfully",
                           data:data,
                           status:200
                         })
                }
              });
            }else{
                res.status(200).json({msg:"No user found"})
            }

        }
    })

  }



//  functions
exports.register = register;
exports.verifyEmail = verifyEmail;
exports.login = login;
exports.forgotPassword = forgotPassword;
exports.resetPassword = resetPassword;
exports.imageUpload = imageUpload;
exports.check = check;
exports.updateMany = updateMany;
exports.getUpdatedValues = getUpdatedValues;
exports.getProfile =getProfile;
exports.userupdate = userupdate;
exports.logout = logout;

exports.createElement = createElement;
exports.createFile = createFile;
exports.createProject =createProject;
